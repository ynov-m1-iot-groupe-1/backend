import { getRepository, MigrationInterface, QueryRunner } from "typeorm";
import { Card } from "../entities/Card";

export class CreateCard1606639122871 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        let card = new Card();
        const cardRepository = getRepository(Card);
        card = await cardRepository.save(card);
        console.log("card", card);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
