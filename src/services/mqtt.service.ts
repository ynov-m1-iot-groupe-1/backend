import * as mqtt from 'mqtt';
import { MqttClient } from 'mqtt';
import config from '../config/config';
import { Card } from '../entities/Card';
import { CardService } from './card.service';
import { SocketService } from './socket.service';

export class MqttService {

  public static client: MqttClient;

  private static CARD_TEMP = "YNOV/DHT11/GAL/TEMP";
  private static CARD_HUM = "YNOV/DHT11/GAL/HUM";
  private static CARD_SENSOIL = "YNOV/SENSOIL_SENSOR/GAL/POURCENTAGE";
  private static CARD_LED = "YNOV/LED/GAL/BOOL";

  private static REQUEST_LED = "YNOV/LED/GAL/BOOL/LISTENING";

  public static init = async () => {

    MqttService.client = mqtt.connect('', config.brokerOpts);

    MqttService.client.on("connect", () => {
      MqttService.client.subscribe([MqttService.CARD_TEMP, MqttService.CARD_HUM, MqttService.CARD_LED, MqttService.CARD_SENSOIL], (err) => {
        if (err) {
          console.log("subscribe error", err);
        }
      });
    });

    MqttService.client.on("message", async (topic: string, message: Buffer) => {

      console.log(`====> MQTT message received on topic [${topic}]: ${message}`);

      let card: Card = await CardService.getCard();
      let updatedCard: Card = { ...card };
      let value: any;

      try {
        value = JSON.parse(message.toString());
      } catch (e) {
        console.log("Error while parsing message", e);
      }

      switch (topic) {
        case MqttService.CARD_TEMP:
          updatedCard.temp = value;
          break;
        case MqttService.CARD_SENSOIL:
          updatedCard.sensoil = value;
          break;
        case MqttService.CARD_HUM:
          updatedCard.hum = value;
          break;
        case MqttService.CARD_LED:
          updatedCard.led = !!value;
          break;
        default:
          console.log("A topic unhandled ?")
          break;
      }

      if (Object.values(card).toString() != Object.values(updatedCard).toString()) {

        const isCardUpdated: boolean = await CardService.updateCard(updatedCard);

        if (isCardUpdated) {
          console.log("[v] Card updated sucessfully.");
          SocketService.sendCard();
        } else {
          console.log("[x] An error occured while updating card.");
        }
      } else {
        console.log("[-] No property to update.");
      }
    });
  }

  public static toggleLed = (led: boolean) => {
    if (!MqttService.client) return;
    MqttService.client.publish(MqttService.REQUEST_LED, led ? '1' : '0');
  }

}