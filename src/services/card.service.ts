import { getRepository } from "typeorm";
import { Card } from "../entities/Card";
import { validate } from "class-validator";

export class CardService {

  public static getCard = async (): Promise<Card> => {
    const cardRepository = getRepository(Card);
    return cardRepository.findOne();
  }

  public static updateCard = async (card: Partial<Card>): Promise<boolean> => {
    delete card.modificationDate;
    const cardRepository = getRepository(Card);
    const errors = await validate(card);
    if (errors.length > 0) {
      console.log("- CardService.updateCard -", errors);
      return false;
    }

    return await cardRepository.update(Card.ref, card).then((response) => {
      return true;
    }).catch((err) => {
      console.log(err);
      return false;
    });
  }
}
