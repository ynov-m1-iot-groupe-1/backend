import { Socket, Server } from "socket.io";
import { Card } from "../entities/Card";
import { CardService } from "./card.service";
import { MqttService } from "./mqtt.service";

export class SocketService {

    public static io: Server;

    private static CARD = "CARD";
    private static LED = "LED";

    public static init = (server: any) => {
        SocketService.io = new Server().listen(server, {
            cors: {
                origin: "*",
                methods: ["GET", "POST"]
            }
        });

        SocketService.io.on("connection", (socket: Socket) => {
            console.log("==> new client connected as " + socket.id);
            SocketService.sendCard(socket);

            socket.on(SocketService.LED, (led: boolean) => {
                MqttService.toggleLed(led);
            });
        });
    }

    public static sendCard = async (socket: Socket = null) => {
        const card: Card = await CardService.getCard();
        if (socket) {
            socket.emit(SocketService.CARD, card);
        } else {
            SocketService.io.emit(SocketService.CARD, card);
        }
    }

}
