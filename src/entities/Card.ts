import { Entity, Column, Unique, PrimaryColumn, UpdateDateColumn } from "typeorm";

@Entity()
@Unique(["name"])
export class Card {

    static readonly ref: string = "ESP8266";

    @PrimaryColumn()
    name: string;

    @UpdateDateColumn({ type: "timestamptz" })
    modificationDate: Date;

    @Column({ type: "decimal", nullable: true })
    temp: number;

    @Column({ type: "decimal", nullable: true })
    hum: number;

    @Column({ type: "decimal", nullable: true })
    sensoil: number;


    @Column({ nullable: true })
    led: boolean;

    constructor(card: Partial<Card> = {}) {
        this.name = Card.ref;
    }
}
